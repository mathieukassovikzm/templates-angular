import { InfosAppEffects } from './infos-app.effects';

export const effects: any[] = [InfosAppEffects];

export * from './infos-app.effects';
